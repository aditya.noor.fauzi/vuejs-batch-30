// SOAL 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var kata1 = pertama.substring(0,4); // saya
var kata2 = pertama.substr(12,6); // senang
var kata3 = kedua.substring(0,7); // belajar
var kata4 = kedua.substr(8,10); // javascript

var upper_kata4  = kata4.toUpperCase(); // JAVASCRIPT

console.log(kata1, kata2, kata3, upper_kata4); // saya senang belajar JAVASCRIPT


// SOAL 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var strInt_1 = parseInt(kataPertama);  // strInt_1 = 10
var strInt_2 = parseInt(kataKedua);  // strInt_2 = 2
var strInt_3 = parseInt(kataKetiga);  // strInt_3 = 4
var strInt_4 = parseInt(kataKeempat);  // strInt_4 = 6

console.log(strInt_1, strInt_2, strInt_3, strInt_4); // 10 2 4 6

var hitung = (strInt_1 * strInt_3) + (strInt_4 / strInt_2); // (10 * 4) + (6 / 2)

console.log(hitung); // 43


// SOAL 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4, 10);  
var kataKetiga = kalimat.substr(15, 3); 
var kataKeempat = kalimat.substr(19, 5);  
var kataKelima = kalimat.substr(25, 6);  

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);


// HASIL
// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali