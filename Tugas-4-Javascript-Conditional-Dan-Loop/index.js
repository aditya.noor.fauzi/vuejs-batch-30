// SOAL 1

var nilai = 75;

if (nilai >= 85){
	console.log('indeksnya A');
} else if (nilai >= 75) {
	console.log('indeksnya B');
} else if (nilai >= 65) {
	console.log('indeksnya C');
} else if (nilai >= 55) {
	console.log('indeksnya D'); 
} else {
	console.log('indeksnya E');
}

console.log('');


// SOAL 2

var tanggal = 11;
var bulan = 3;
var tahun = 1997;

switch(bulan) {
  case 1:    { bulan = "Januari"; break;}
  case 2:    { bulan = "Februari"; break;}
  case 3:    { bulan = "Maret"; break;}
  case 4:    { bulan = "April"; break;}
  case 5:    { bulan = "Mei"; break;}
  case 6:    { bulan = "Juni"; break;}
  case 7:    { bulan = "Juli"; break;}
  case 8:    { bulan = "Agustus"; break;}
  case 9:    { bulan = "September"; break;}
  case 10:   { bulan = "Oktober"; break;}
  case 11:   { bulan = "November"; break;}
  case 12:   { bulan = "Desember"; break;}
  default:  { console.log('Angka bulan salah');}
}

console.log(tanggal, bulan, tahun);

console.log('');

// SOAL 3

var n = 3;

console.log('Output untuk n = 3 :');
console.log('');

for(i=1; i<=n; i++)
    {
        for(j=1; j<=i; j++)
        {
            process.stdout.write('#');
        }

    console.log('');

    }

console.log('');

var n = 7;

console.log('Output untuk n = 7 :');
console.log('');

for(i=1; i<=n; i++)
    {
        for(j=1; j<=i; j++)
        {
            process.stdout.write('#');
        }
        
    console.log('');

    }

console.log('');


// SOAL 4

var a = "I love programming";
var b = "I love Javascript";
var c = "I love VueJS";

console.log('Output untuk m = 3 :')
var m = 3;

var num = 1;

var count = 1;

while(num<=m){

	if(count==1){
		console.log(num,'-',a);
	} else if (count==2) {
		console.log(num,'-',b);
	} else {
		console.log(num,'-',c);
	}

	count++;

	if(num%3==0){

		for(j=1; j<=num; j++)
        {
            process.stdout.write('=');
        }

        console.log('');
		count=1;
	}

	num++;

}

console.log('');

console.log('Output untuk m = 5 :')
var m = 5;

var num = 1;

var count = 1;

while(num<=m){

	if(count==1){
		console.log(num,'-',a);
	} else if (count==2) {
		console.log(num,'-',b);
	} else {
		console.log(num,'-',c);
	}

	count++;

	if(num%3==0){

		for(j=1; j<=num; j++)
        {
            process.stdout.write('=');
        }

        console.log('');
		count=1;
	}

	num++;

}

console.log('');

console.log('Output untuk m = 7 :')
var m = 7;

var num = 1;

var count = 1;

while(num<=m){

	if(count==1){
		console.log(num,'-',a);
	} else if (count==2) {
		console.log(num,'-',b);
	} else {
		console.log(num,'-',c);
	}

	count++;

	if(num%3==0){

		for(j=1; j<=num; j++)
        {
            process.stdout.write('=');
        }

        console.log('');
		count=1;
	}

	num++;

}

console.log('');

console.log('Output untuk m = 10 :')
var m = 10;

var num = 1;

var count = 1;

while(num<=m){

	if(count==1){
		console.log(num,'-',a);
	} else if (count==2) {
		console.log(num,'-',b);
	} else {
		console.log(num,'-',c);
	}

	count++;

	if(num%3==0){

		for(j=1; j<=num; j++)
        {
            process.stdout.write('=');
        }

        console.log('');
		count=1;
	}

	num++;

}
