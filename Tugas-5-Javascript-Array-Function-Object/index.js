// SOAL 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var sortedDaftarHewan = daftarHewan.sort();

var panjangArray = sortedDaftarHewan.length;

for (var i = 0; i < panjangArray; i++) {
	console.log(sortedDaftarHewan[i]);
}

console.log('');


// SOAL 2

function introduce(mydata) {
  var hasil = "Nama saya %s, umur saya %d, alamat saya %s, dan saya punya hobby yaitu %s";
  return hasil;
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };

var nama   = data.name;
var umur   = data.age;
var alamat = data.address;
var hobi   = data.hobby;
 
var perkenalan = introduce(data);
console.log(perkenalan, nama, umur, alamat, hobi);

console.log('');


// SOAL 3

function hitung_huruf_vokal(str) {

	var vokal = 0;

	for (var i = 0; i < str.length; i++)
	{
	  if (str[i] >= "A" && str[i] <= "Z") vokal++;
	}

	return vokal;
}

var str1 = "Aditya Noor Fauzi";
var str2 = "Aditya Noor";

var hitung_1 = hitung_huruf_vokal(str1);

var hitung_2 = hitung_huruf_vokal(str2);

console.log(hitung_1 , hitung_2); 

console.log('');


// SOAL 4

function hitung(angka) {
	var hasil = -2 + (angka*2);
	return hasil;
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8