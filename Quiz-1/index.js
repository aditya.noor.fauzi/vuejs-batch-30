// SOAL 1

function myBulan(bln) {
	switch(bln) {
	  case 1:    { bln = "Januari"; break;}
	  case 2:    { bln = "Februari"; break;}
	  case 3:    { bln = "Maret"; break;}
	  case 4:    { bln = "April"; break;}
	  case 5:    { bln = "Mei"; break;}
	  case 6:    { bln = "Juni"; break;}
	  case 7:    { bln = "Juli"; break;}
	  case 8:    { bln = "Agustus"; break;}
	  case 9:    { bln = "September"; break;}
	  case 10:   { bln = "Oktober"; break;}
	  case 11:   { bln = "November"; break;}
	  case 12:   { bln = "Desember"; break;}
	  default:  { console.log('Angka bulan salah');}
	}

	return bln;
}

function next_date(tanggal, bulan, tahun){

	if (tanggal == 31 && bulan == 12){
	  tanggal = 1;
	  bulan = 1;
	  tahun = tahun + 1;
	  bulan = myBulan(bulan);

	} else if (tanggal == 30 && Math.abs(bulan % 2) == 1){
		tanggal = 1;
		bulan = bulan + 1;
		bulan = myBulan(bulan);

	} else if (tanggal == 31 && bulan % 2 == 0){
		tanggal = 1;
		bulan = bulan + 1;
		bulan =myBulan(bulan);

	} else if (tanggal == 28 && bulan == 2 && tahun % 4 == 0){
		tanggal = tanggal + 1;
		bulan =myBulan(bulan);

	} else if (tanggal == 28 && bulan == 2 && tahun % 4 != 0){
		tanggal = 1;
		bulan = bulan + 1;
		bulan = myBulan(bulan);

	} else if (tanggal == 29 && bulan == 2 && tahun % 4 == 0){
		tanggal = 1;
		bulan = bulan + 1;
		bulan = myBulan(bulan);

	} else if (tanggal > 30 && Math.abs(bulan % 2) == 1 ){
	  console.log('invalid tanggal input');

	} else if (tanggal > 31 && bulan % 2 == 0 ){
		console.log('invalid tanggal input');

	} else if ( tanggal > 29 && bulan == 2 && tahun % 4 != 0){
		console.log('invalid tanggal input');

	} else if (tanggal > 28 && bulan == 2 && tahun % 4 == 0){
		console.log('invalid tanggal input');

	} else {
		tanggal = tanggal + 1;
		bulan = myBulan(bulan);
	}

	var date = [ tanggal, " ", bulan, " ", tahun ];

	var joinedDate = date.join("");

	console.log(joinedDate);

}

next_date(28,2,2020);
next_date(28,2,2007);
next_date(29,2,2020);
next_date(11,3,1997);
next_date(31,4,1997);
next_date(30,3,1997);
next_date(31,12,1997);
next_date(30,12,1997);

console.log();


//SOAL 2

function jumlah_kata(str){

	str = str.replace(/(^\s*)|(\s*$)/gi,"");
	str = str.replace(/[ ]{2,}/gi," ");
	str = str.replace(/\n /,"\n");
	return str.split(' ').length;

}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));
console.log(jumlah_kata(kalimat_3));