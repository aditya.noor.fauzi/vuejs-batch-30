// SOAL 1

const kelPersegiPanjang = (panjang, lebar) => panjang*2 + lebar*2;


const luasPersegiPanjang = (panjang, lebar) => panjang * lebar;

console.log("Keliling persegi panjang =", kelPersegiPanjang(10,5));
console.log("Luas persegi panjang =",luasPersegiPanjang(10,5));

console.log('');


// SOAL 2

const literal = (firstName, lastName) => {
	return {
    firstName,
    lastName,

    fullName(){ 
    	return console.log(firstName + " " + lastName) 
    }
  };
}

// Driver code
literal("William", "Imoh").fullName();

console.log('');


// SOAL 3

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;

// Driver code
console.log(firstName, lastName, address, hobby)

console.log('');


// SOAL 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east];

//Driver Code
console.log(combined);

console.log('');


// SOAL 5

let planet = "earth" 
let view = "glass" 

let myString = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(myString);

console.log('');