/*var readBooks = require('./callback.js')*/
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini

const myCallback = (sisaWaktu) => {
  console.log(`sisa waktu anda ${sisaWaktu}`);
  if(sisaWaktu <= 0){
    console.log('waktu selesai');
  } else {
    // setTimeout(() => {
      getBooks(sisaWaktu)
    // }, 0000)
  }
}

const getBooks = (myTime) => {
  setTimeout(() => {
    books.forEach((book,index) => {
      readBooks(myTime, book, myCallback );
    })
  }, 1000)
  
}

function readBooks(time, book, callback ) {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function(){
        let sisaWaktu = 0
        console.log(`waktu buku ${book.timeSpent}`)
        if(time >= book.timeSpent) {
            sisaWaktu = time - book.timeSpent
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu) //menjalankan function callback
        } else {
            console.log('waktu saya habis')
            callback(time)
        }   
    }, book.timeSpent)
}


getBooks(10000);